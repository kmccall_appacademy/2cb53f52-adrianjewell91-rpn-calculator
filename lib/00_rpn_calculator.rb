class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_operation(:+)
  end

    def times
      perform_operation(:*)
    end

    def minus
      perform_operation(:-)
    end

    def divide
    perform_operation(:/)
    end

    def value
      @stack.last
    end

    def tokens(str) #Don't take potential typos error into account.
          str.split.map do |el|
            unless ("+-*/").include?(el)
              el.to_i
            else
              el.to_sym
            end
          end
    end

    def evaluate(str)
      a_stack = tokens(str)
      a_stack.each do |el|
        case is_operation?(el)
        when true
          perform_operation(el)
        else
          push(el)
        end
      end

      value
    end

    private

    def is_operation?(element)
      [:+,:-,:*,:/].include?(element)
    end

    def perform_operation(symbol)
      raise "calculator is empty" if @stack.size < 2

      first_operand = @stack.pop
      second_operand = @stack.pop
      case symbol
      when :+
        @stack << second_operand + first_operand
      when :-
        @stack << second_operand - first_operand
      when :*
        @stack << second_operand * first_operand
      when :/
        @stack << second_operand.fdiv(first_operand)
      else
        @stack << second_operand
        @stack << first_operand
        raise "#{symbol} is not a correct symbol. Please try again."
      end
    end
end

#To ref